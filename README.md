# Flutter Get Arch Template

A new Flutter project template using Get package and having some general Services implemented for you.

## Getting Started

Before you download make sure you have Home Brew installed on your machine.

After downloading, you should navigate to the dowloaded project using Command line.  
Once you are in the directory, run the following command providing the -n flag and giving a name to your project.

`./bootstrap -n my_awesome_project`

Once the script is done, move the created directory (in our case my_awesome_project) to a directory you wish.
Open the created project with your preferable IDE and enjoy ;)