import 'package:get/get.dart';

abstract class AuthenticationBase extends GetxService {
  // Properties
  bool get isUserAuthenticated;
  GetStream<bool> get authChangeStream;

  // Methods
  Future<void> login();
  Future<void> logout();
}
