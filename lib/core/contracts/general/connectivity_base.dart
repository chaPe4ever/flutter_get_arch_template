import 'dart:async';

import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/enums/enum_connectivity_status.dart';

abstract class ConnectivityBase extends GetxService {
  Future<bool> get isConnected;
  StreamController<EnumConnectivityStatus> get statusStreamController;
}
