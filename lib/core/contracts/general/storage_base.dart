import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class StorageBase extends GetxService {
  Future<StorageBase> init({String container = 'GetStorage'});
  // Write
  /// Write data on your container
  Future<void> write(String key, dynamic value);
  void writeInMemory(String key, dynamic value);
  Future<void> writeIfNull(String key, dynamic value);

  // Read
  /// Reads a value in your container with the given key.
  T? read<T>(String key);

  // Delete
  /// remove data from container by key
  Future<void> remove(String key);

  /// clear all data on your container
  Future<void> erase();

  // Listeners
  /// Listen changes in your container
  VoidCallback listen(VoidCallback value);
  VoidCallback listenKey(String key, ValueSetter callback);
}
