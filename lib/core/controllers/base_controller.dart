import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/contracts/general/authentication_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/connectivity_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/dialog_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/navigation_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/storage_base.dart';

mixin BaseController on GetxController {
  // Fields
  RxBool _isBusy = false.obs;
  final ConnectivityBase _connectivityService = Get.find();
  final StorageBase _storage = Get.find();
  final NavigationBase _navigationService = Get.find();
  final DialogBase _dialogService = Get.find();
  final AuthenticationBase _authenticationService = Get.find();

  // Getters
  bool get isBusy => _isBusy.value;
  ConnectivityBase get connectivityService => _connectivityService;
  StorageBase get storageService => _storage;
  NavigationBase get navigationService => _navigationService;
  DialogBase get dialogService => _dialogService;
  AuthenticationBase get authenticationService => _authenticationService;

  // Setters
  set isBusy(bool value) {
    _isBusy.value = value;
  }
}
