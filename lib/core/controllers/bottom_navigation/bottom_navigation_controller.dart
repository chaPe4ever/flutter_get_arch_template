import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/enums/enum_connectivity_status.dart';
import 'package:pub_tran_exercise/globals/helpers/theme_helper.dart';
import 'package:pub_tran_exercise/globals/theme/app_colors.dart';
import 'package:pub_tran_exercise/ui/views/home/home_view.dart';
import 'package:pub_tran_exercise/ui/views/settings_view/settings_view.dart';

import '../base_controller.dart';

class BottomNavigationController extends SuperController with BaseController {
  // Fields
  RxInt _currentNavPageIndex = 0.obs;
  RxString _appBarTitle = ''.obs;
  late List<BottomNavigationBarItem> _bottomNavigationBarItems;
  late List<String> _bottomNavPageNames;
  Rxn<SystemUiOverlayStyle> _systemUiOverlayStyle = Rxn();

  // Getters
  int get currentNavPageIndex => _currentNavPageIndex.value;
  List<BottomNavigationBarItem> get bottomNavigationBarItems => _bottomNavigationBarItems;
  List<String> get bottomNavPageNames => _bottomNavPageNames;
  String get appBarTitle => _appBarTitle.value;
  SystemUiOverlayStyle? get systemUiOverlayStyle => _systemUiOverlayStyle.value;

  // Setters
  void setCurrentNavPageIndex(int index) {
    _currentNavPageIndex.value = index;
    _appBarTitle.value = _bottomNavigationBarItems[index].label ?? '';
    navigationService.toNamed(_bottomNavPageNames[index], id: 1);
  }

  set systemUiOverlayStyle(SystemUiOverlayStyle? value) {
    _systemUiOverlayStyle.value = value;
  }

  // Initialisation - LifeCycle
  @override
  void onInit() async {
    super.onInit();

    try {
      _bottomNavigationBarItems = populateNavBar(withHeight: 36.0);
      _bottomNavPageNames = populateNavBarNames();
      _appBarTitle.value = _bottomNavigationBarItems[currentNavPageIndex].label ?? '';

      // Listen to connection changes
      connectivityService.statusStreamController.stream.distinct().listen((enumConnectivityStatus) {
        if (enumConnectivityStatus == EnumConnectivityStatus.Offline) {
          systemUiOverlayStyle = SystemUiOverlayStyle(statusBarColor: AppColors.like);
          if (!navigationService.isSnackbarOpen) {
            dialogService.snackbar('offline'.tr, backgroundColor: AppColors.like);
          }
        } else {
          systemUiOverlayStyle = SystemUiOverlayStyle(
            statusBarColor: ThemeHelper.isDarkMode ? AppColors.dark : AppColors.light,
          );
        }
        printInfo(info: enumConnectivityStatus.toString());
      }).onError((e) => printError(info: 'Exception on connectivityService: ${e.toString()}'));
    } catch (e) {
      printError(info: e.toString());
    }
  }

  @override
  void onDetached() {
    printInfo(info: 'onDetached');
  }

  @override
  void onInactive() {
    printInfo(info: 'onInactive');
  }

  @override
  void onPaused() {
    printInfo(info: 'onPaused');
  }

  @override
  void onResumed() {
    printInfo(info: 'onResumed');
  }

  @override
  void onClose() {
    super.onClose();
  }

  // Methods

  List<BottomNavigationBarItem> populateNavBar({required double withHeight}) {
    return [
      BottomNavigationBarItem(
        icon: Icon(Icons.home_outlined),
        activeIcon: Icon(Icons.home),
        label: 'Home',
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.settings_applications_outlined),
        activeIcon: Icon(Icons.settings_applications),
        label: 'settings'.tr,
      ),
    ];
  }

  // Should always match the sequence of [populateNavBar]
  List<String> populateNavBarNames() {
    return [HomeView.route, SettingsView.route];
  }
}
