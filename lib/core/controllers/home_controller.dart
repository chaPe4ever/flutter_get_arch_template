import 'package:get/get.dart';

import 'base_controller.dart';

class HomeController extends SuperController with BaseController {
  // Fields

  // Getters

  // Setters

  // Initialisation - LifeCycle
  @override
  void onInit() async {
    isBusy = true;
    super.onInit();
    try {} catch (e) {
      printError(info: e.toString());
    } finally {
      isBusy = false;
    }
  }

  @override
  void onDetached() {
    printInfo(info: 'onDetached');
  }

  @override
  void onInactive() {
    printInfo(info: 'onInactive');
  }

  @override
  void onPaused() {
    printInfo(info: 'onPaused');
  }

  @override
  void onResumed() {
    printInfo(info: 'onResumed');
  }

  @override
  void onClose() {
    super.onClose();
  }

  // Methods

}
