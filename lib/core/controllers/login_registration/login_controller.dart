import 'dart:async';

import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/enums/enum_connectivity_status.dart';
import 'package:pub_tran_exercise/globals/helpers/theme_helper.dart';
import 'package:pub_tran_exercise/globals/theme/app_colors.dart';
import 'package:pub_tran_exercise/ui/views/bottom_navigation/bottom_navigation_view.dart';

import '../base_controller.dart';

class LoginController extends SuperController with BaseController {
  // Fields
  Rxn<SystemUiOverlayStyle> _systemUiOverlayStyle = Rxn();

  // Getters
  SystemUiOverlayStyle? get systemUiOverlayStyle => _systemUiOverlayStyle.value;

  // Setters
  set systemUiOverlayStyle(SystemUiOverlayStyle? value) {
    _systemUiOverlayStyle.value = value;
  }

  // Initialisation - LifeCycle
  @override
  void onInit() async {
    isBusy = true;
    super.onInit();
    try {
      // Listen to connection changes
      connectivityService.statusStreamController.stream.distinct().listen((enumConnectivityStatus) {
        if (enumConnectivityStatus == EnumConnectivityStatus.Offline) {
          systemUiOverlayStyle = SystemUiOverlayStyle(
            statusBarColor: AppColors.like,
          );
          if (!navigationService.isSnackbarOpen) {
            dialogService.snackbar('offline'.tr, backgroundColor: AppColors.like);
          }
        } else {
          systemUiOverlayStyle =
              SystemUiOverlayStyle(statusBarColor: ThemeHelper.isDarkMode ? AppColors.dark : AppColors.light);
        }
      }).onError((e) => printError(info: 'Exception on connectivityService: ${e.toString()}'));
    } catch (e) {
      printError(info: e.toString());
    } finally {
      isBusy = false;
    }
  }

  @override
  void onDetached() {
    printInfo(info: 'onDetached');
  }

  @override
  void onInactive() {
    printInfo(info: 'onInactive');
  }

  @override
  void onPaused() {
    printInfo(info: 'onPaused');
  }

  @override
  void onResumed() {
    printInfo(info: 'onResumed');
  }

  @override
  void onClose() {
    super.onClose();
  }

// Methods
  Future<void> login() async {
    try {
      isBusy = true;
      await authenticationService.login();
      if (authenticationService.isUserAuthenticated) {
        navigationService.offAllNamed(BottomNavigationView.route);
      }
    } catch (e) {
      printError(info: e.toString());
    } finally {
      isBusy = false;
    }
  }
}
