import 'dart:async';

import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/controllers/base_controller.dart';
import 'package:pub_tran_exercise/ui/views/login_registration/login_view.dart';

class SettingsController extends SuperController with BaseController {
  // Fields

  // Getters

  // Setters

  // Initialisation - LifeCycle
  @override
  void onInit() async {
    isBusy = true;
    super.onInit();
    try {} catch (e) {
      printError(info: e.toString());
    } finally {
      isBusy = false;
    }
  }

  @override
  void onDetached() {
    printInfo(info: 'onDetached');
  }

  @override
  void onInactive() {
    printInfo(info: 'onInactive');
  }

  @override
  void onPaused() {
    printInfo(info: 'onPaused');
  }

  @override
  void onResumed() {
    printInfo(info: 'onResumed');
  }

  @override
  void onClose() {
    super.onClose();
  }

// Methods
  FutureOr<void> logout() async {
    try {
      isBusy = true;
      await authenticationService.logout();
      if (!authenticationService.isUserAuthenticated) {
        navigationService.offAllNamed(LoginView.route);
      }
    } catch (e) {
      printError(info: e.toString());
    } finally {
      isBusy = false;
    }
  }
}
