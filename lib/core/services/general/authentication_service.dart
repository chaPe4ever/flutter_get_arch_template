import 'package:get/get.dart';

import '../../contracts/general/authentication_base.dart';

class AuthenticationService extends AuthenticationBase {
  // Fields
  var _isUserAuthenticated = false;
  final _authChangeStream = GetStream<bool>();

  // Getters
  @override
  bool get isUserAuthenticated => _isUserAuthenticated;
  @override
  GetStream<bool> get authChangeStream => _authChangeStream;

  // Setters

  // Init - Ctr
  @override
  void onInit() {
    super.onInit();

    _isUserAuthenticated = true;
  }

  // Methods
  @override
  Future<void> login() async {
    try {
      _isUserAuthenticated = true;
      _authChangeStream.add(_isUserAuthenticated);
    } catch (e) {
      printError(info: e.toString());
    }
  }

  @override
  Future<void> logout() async {
    try {
      _isUserAuthenticated = false;
      _authChangeStream.add(_isUserAuthenticated);
    } catch (e) {
      printError(info: e.toString());
    }
  }

  // Private methods
}
