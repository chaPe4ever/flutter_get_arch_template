import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:pub_tran_exercise/core/contracts/general/connectivity_base.dart';
import 'package:pub_tran_exercise/core/enums/enum_connectivity_status.dart';

class ConnectivityService extends ConnectivityBase {
  // Fields
  late final StreamSubscription<ConnectivityResult> _subscription;
  late final StreamController<EnumConnectivityStatus> _statusStreamController;

  // Initialisation
  ConnectivityService() {
    _statusStreamController = StreamController<EnumConnectivityStatus>.broadcast();
    // Subscribe to the connectivity Changed Steam
    _subscription = Connectivity().onConnectivityChanged.distinct().listen((ConnectivityResult result) {
      _statusStreamController.add(_getStatusFromResult(result));
    });
  }

  @override
  void onClose() {
    _statusStreamController.close();
    _subscription.cancel();
  }

  @override
  Future<bool> get isConnected async {
    final connectivityResult = await Connectivity().checkConnectivity();

    return connectivityResult != ConnectivityResult.none;
  }

  @override
  StreamController<EnumConnectivityStatus> get statusStreamController => _statusStreamController;

  // Methods
  // Convert from the third part enum to our own enum
  EnumConnectivityStatus _getStatusFromResult(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.mobile:
        return EnumConnectivityStatus.Cellular;
      case ConnectivityResult.wifi:
        return EnumConnectivityStatus.WiFi;
      case ConnectivityResult.none:
        return EnumConnectivityStatus.Offline;
      default:
        return EnumConnectivityStatus.Offline;
    }
  }
}
