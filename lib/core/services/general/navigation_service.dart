import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/contracts/general/navigation_base.dart';

class NavigationService extends NavigationBase {
  @override
  dynamic get arguments => Get.arguments;

  @override
  String get previousRoute => Get.previousRoute;

  @override
  String get currentRoute => Get.currentRoute;

  @override
  Route<dynamic>? get rawRoute => Get.rawRoute;

  @override
  Routing get routing => Get.routing;

  @override
  bool? get isBottomSheetOpen => Get.isBottomSheetOpen;

  @override
  bool? get isDialogOpen => Get.isDialogOpen;

  @override
  bool get isSnackbarOpen => Get.isSnackbarOpen;

  @override
  Future<T?>? offAllNamed<T>(
    String newRouteName, {
    RoutePredicate? predicate,
    dynamic arguments,
    int? id,
    Map<String, String>? parameters,
  }) async =>
      await Get.offAllNamed(
        newRouteName,
        predicate: predicate,
        arguments: arguments,
        id: id,
        parameters: parameters,
      );

  @override
  Future<T?>? toNamed<T>(
    String page, {
    dynamic arguments,
    int? id,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
  }) async =>
      await Get.toNamed(
        page,
        arguments: arguments,
        id: id,
        preventDuplicates: preventDuplicates,
        parameters: parameters,
      );

  @override
  void back<T>({T? result, bool closeOverlays = false, bool canPop = true, int? id}) =>
      Get.back<T>(result: result, closeOverlays: closeOverlays, canPop: canPop, id: id);

  @override
  void removeRoute(Route<dynamic> route, {int? id}) => Get.removeRoute(route, id: id);

  @override
  void until(RoutePredicate predicate, {int? id}) => Get.until(predicate, id: id);

  @override
  Future<T?>? offUntil<T>(Route<T> page, RoutePredicate predicate, {int? id}) async =>
      await Get.offUntil<T>(page, predicate, id: id);

  @override
  Future<T?>? offNamedUntil<T>(String page, RoutePredicate predicate,
          {int? id, dynamic arguments, Map<String, String>? parameters}) async =>
      await Get.offNamedUntil(page, predicate, id: id, arguments: arguments, parameters: parameters);

  @override
  Future<T?>? offAndToNamed<T>(
    String page, {
    dynamic arguments,
    int? id,
    dynamic result,
    Map<String, String>? parameters,
  }) async =>
      await Get.offAndToNamed(page, arguments: arguments, id: id, result: result, parameters: parameters);

  @override
  Future<T?>? offNamed<T>(String page,
          {arguments, int? id, bool preventDuplicates = true, Map<String, String>? parameters}) async =>
      Get.offNamed(page, arguments: arguments, id: id, preventDuplicates: preventDuplicates, parameters: parameters);
}
