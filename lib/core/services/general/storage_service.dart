import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:pub_tran_exercise/core/contracts/general/storage_base.dart';

class StorageService extends StorageBase {
  @override
  Future<StorageBase> init({String container = 'GetStorage'}) async {
    await GetStorage.init(container = container);
    return this;
  }

  @override
  Future<void> erase() => GetStorage().erase();

  @override
  VoidCallback listen(VoidCallback value) => GetStorage().listen(value);

  @override
  VoidCallback listenKey(String key, ValueSetter callback) => GetStorage().listenKey(key, callback);

  @override
  T? read<T>(String key) => GetStorage().read<T>(key);

  @override
  Future<void> remove(String key) => GetStorage().remove(key);

  @override
  Future<void> write(String key, value) => GetStorage().write(key, value);

  @override
  Future<void> writeIfNull(String key, value) => GetStorage().writeIfNull(key, value);

  @override
  void writeInMemory(String key, value) => GetStorage().writeInMemory(key, value);
}
