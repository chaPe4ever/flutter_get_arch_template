/// REST Api
const String kBaseUrl = 'https://your_base_url.de';
const String kTokenUrl = 'https://your_token_url';

/// GetStorage keys
const String kApiAuthToken = 'apiAuthToken';

/// Strings-Sentences
const String kConnectionError = 'Connection  Error';
const String kConnectionMsg =
    'Your connection is down. Please make sure you are connected to the internet';

/// Asset paths
const kExampleAssetPath = 'lib/assets/images/example.png';

/// Test keys

/// Test strings
