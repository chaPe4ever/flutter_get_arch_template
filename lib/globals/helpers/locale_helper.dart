import 'package:get/get.dart';

class LocaleHelper {
  static String? get deviceLocale => Get.deviceLocale?.languageCode.toLowerCase();

  static String currentLocale({required String Function() onEn, required String Function() onFallback}) {
    if (deviceLocale == 'en') {
      return onEn.call();
    } else {
      return onFallback.call();
    }
  }

  static String onCurrentLocale({required String en, required String fallback}) {
    if (deviceLocale == 'en') {
      return en;
    } else {
      return fallback;
    }
  }
}
