import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MediaQueryHelper {
  ///The vertical extent of this size
  static double get height => Get.height;

  ///The horizontal extent of this size.
  static double get width => Get.width;

  ///The distance from the top edge to the first unpadded pixel,
  ///in physical pixels.
  static double get statusBarHeight => Get.statusBarHeight;

  ///The distance from the bottom edge to the first unpadded pixel,
  ///in physical pixels.
  static double get bottomBarHeight => Get.bottomBarHeight;

  ///The system-reported text scale.
  static double get textScaleFactor => Get.textScaleFactor;

  /// give access to Mediaquery.of(context)
  static MediaQueryData get mediaQuery => Get.mediaQuery;

  /// give access to Mediaquery.of(context).size
  static Size get mediaQuerySize => mediaQuery.size;
}
