import 'package:get/get.dart';

class PlatformHelper {
  static bool get isAndroid => GetPlatform.isAndroid;
  static bool get isIOS => GetPlatform.isIOS;
  static bool get isMacOS => GetPlatform.isMacOS;
  static bool get isWindows => GetPlatform.isWindows;
  static bool get isLinux => GetPlatform.isLinux;
  static bool get isFuchsia => GetPlatform.isFuchsia;
  static bool get isMobile => GetPlatform.isMobile;
  static bool get isDesktop => GetPlatform.isDesktop;
  static bool get isWeb => GetPlatform.isWeb;
}
