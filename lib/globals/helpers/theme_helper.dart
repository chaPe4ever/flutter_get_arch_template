import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ThemeHelper {
  /// give access to Theme.of(context)
  static ThemeData get theme => Get.theme;

  /// give access to TextTheme.of(context)
  static TextTheme get textTheme => Get.textTheme;

  /// give access to Mediaquery.of(context)
  static MediaQueryData get mediaQuery => Get.mediaQuery;

  /// Check if dark mode theme is enable
  static bool get isDarkMode => Get.isDarkMode;

  /// Check if light mode theme is enable
  static bool get isLightMode => theme.brightness == Brightness.light;

  /// Check if dark mode theme is enable on platform on android Q+
  static bool get isPlatformDarkMode => Get.isPlatformDarkMode;

  /// give access to Theme.of(context).iconTheme.color
  static Color? get iconColor => Get.iconColor;
}
