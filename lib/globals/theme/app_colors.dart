import 'package:flutter/material.dart';

abstract class AppColors {
  /// Dark color.
  static const dark = Colors.black;

  static const light = Color(0xFFFAFAFA);

  /// Grey background accent.
  static const grey = Color(0xFF262626);

  static Color greyLight = Colors.grey[800] ?? Colors.transparent;

  /// Primary text color
  static const primaryText = Colors.white;

  /// Secondary color.
  static const secondary = faded;

  /// Color to use for favorite icons (indicating a like).
  static const like = Colors.red;

  /// Grey faded color.
  static const faded = Colors.grey;

  /// Light grey color
  static const ligthGrey = Color(0xFFEEEEEE);

  /// Top gradient color used in various UI components.
  static const topGradient = Color(0xFFE60064);

  /// Bottom gradient color used in various UI components.
  static const bottomGradient = Color(0xFFFFB344);

  static const primaryDark = light;
  static const primaryLight = dark;
}

class AppColorScheme {
  static ColorScheme get light => ColorScheme.light().copyWith(primary: AppColors.primaryLight);
  static ColorScheme get dark => ColorScheme.dark().copyWith(primary: AppColors.primaryDark);
}
