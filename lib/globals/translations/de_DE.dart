Map<String, String> get de_DE => {
      'settings': 'Einstellungen',
      'login': 'Einloggen',
      'logout': 'Ausloggen',
      'offline': 'Ihre Verbindung zum Internet wurde unterbrochen!',
    };
