Map<String, String> get en_US => {
      'settings': 'Settings',
      'login': 'Login',
      'logout': 'Logout',
      'offline': 'Your connection to the internet has been lost!',
    };
