import 'package:get/get.dart';
import 'package:pub_tran_exercise/globals/translations/de_DE.dart';
import 'package:pub_tran_exercise/globals/translations/en_US.dart';

class TranslationsService extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {'en_US': en_US, 'de_DE': de_DE};
}
