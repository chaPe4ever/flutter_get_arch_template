import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/ui/utils/initial_config.dart';
import 'package:pub_tran_exercise/ui/utils/page_router.dart';
import 'package:pub_tran_exercise/globals/theme/app_theme.dart';
import 'package:pub_tran_exercise/globals/translations/translations_service.dart';
import 'package:pub_tran_exercise/ui/views/login_registration/login_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialConfig();

  // Allow only portrait mode on Android
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    translations: TranslationsService(),
    locale: Get.deviceLocale,
    fallbackLocale: const Locale('en', 'US'),
    theme: Get.find<AppTheme>().lightTheme,
    darkTheme: Get.find<AppTheme>().darkTheme,
    initialRoute: LoginView.route,
    getPages: PageRouter.getPages,
    defaultTransition: Transition.fade,
  ));
}
