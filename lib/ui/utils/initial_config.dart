import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/contracts/general/api_auth_provider_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/authentication_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/connectivity_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/dialog_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/navigation_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/storage_base.dart';
import 'package:pub_tran_exercise/core/services/general/api_auth_provider_service.dart';
import 'package:pub_tran_exercise/core/services/general/authentication_service.dart';
import 'package:pub_tran_exercise/core/services/general/connectivity_service.dart';
import 'package:pub_tran_exercise/core/services/general/dialog_service.dart';
import 'package:pub_tran_exercise/core/services/general/navigation_service.dart';
import 'package:pub_tran_exercise/core/services/general/storage_service.dart';
import 'package:pub_tran_exercise/globals/theme/app_theme.dart';

Future<void> initialConfig() async {
  // App internal
  await Get.putAsync<StorageBase>(() async => await StorageService().init());
  Get.put<NavigationBase>(NavigationService(), permanent: true);
  Get.put<DialogBase>(DialogService(), permanent: true);
  Get.put<AppTheme>(AppTheme(), permanent: true);
  Get.put<ConnectivityBase>(ConnectivityService(), permanent: true);
  Get.put<AuthenticationBase>(AuthenticationService(), permanent: true);
  Get.lazyPut<ApiAuthProviderBase>(() => ApiAuthProviderService(), fenix: true);
}
