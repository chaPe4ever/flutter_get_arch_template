import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/contracts/general/authentication_base.dart';
import 'package:pub_tran_exercise/ui/views/bottom_navigation/bottom_navigation_view.dart';
import 'package:pub_tran_exercise/ui/views/login_registration/login_view.dart';

class AuthMiddleware extends GetMiddleware {
  final AuthenticationBase _authService = Get.find();

  @override
  RouteSettings? redirect(String? route) {
    // Redirect if the user is not any longer authenticated. Would be nice to add also a Snackbar
    switch (route) {
      case LoginView.route:
        return _authService.isUserAuthenticated ? RouteSettings(name: BottomNavigationView.route) : null;
      default:
        return _authService.isUserAuthenticated ? null : RouteSettings(name: LoginView.route);
    }
  }

  @override
  GetPage? onPageCalled(GetPage? page) {
    // return _authService.username != null ? page.copyWith(parameter: {'user': authController.username}) : page;
    return page;
  }

  @override
  List<Bindings>? onBindingsStart(List<Bindings>? bindings) {
    // This function will be called right before the Bindings are initialize,
    // then bindings is null
    return bindings;
  }

  @override
  GetPageBuilder? onPageBuildStart(GetPageBuilder? page) {
    return page;
  }

  @override
  Widget onPageBuilt(Widget page) {
    return page;
  }

  @override
  void onPageDispose() {}
}
