import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/controllers/bottom_navigation/bottom_navigation_controller.dart';
import 'package:pub_tran_exercise/core/controllers/home_controller.dart';
import 'package:pub_tran_exercise/core/controllers/login_registration/login_controller.dart';
import 'package:pub_tran_exercise/core/controllers/settings/settings_controller.dart';
import 'package:pub_tran_exercise/ui/utils/middlewares/auth_middleware.dart';
import 'package:pub_tran_exercise/ui/views/bottom_navigation/bottom_navigation_view.dart';
import 'package:pub_tran_exercise/ui/views/home/home_view.dart';
import 'package:pub_tran_exercise/ui/views/login_registration/login_view.dart';
import 'package:pub_tran_exercise/ui/views/settings_view/settings_view.dart';

class PageRouter {
  static final getPages = [
    GetPage(
      name: LoginView.route,
      page: () => LoginView(),
      binding: BindingsBuilder(() {
        Get.lazyPut(() => LoginController());
      }),
      middlewares: [AuthMiddleware()],
    ),
    GetPage(
      name: BottomNavigationView.route,
      page: () => BottomNavigationView(),
      binding: BindingsBuilder(() {
        Get.lazyPut(() => BottomNavigationController());
      }),
      children: [
        GetPage(
          name: HomeView.route,
          page: () => HomeView(),
          binding: homeBinding,
        ),
        GetPage(
          name: SettingsView.route,
          page: () => SettingsView(),
          binding: settingsBinding,
        ),
      ],
      middlewares: [AuthMiddleware()],
    ),
  ];

  // Shared Bindings
  static Bindings get homeBinding => BindingsBuilder(() {
        Get.lazyPut(() => HomeController());
      });

  static Bindings get settingsBinding => BindingsBuilder(() {
        Get.lazyPut(() => SettingsController());
      });

  // BottomNav pageRoutes
  static Route? onGenerateRoute(RouteSettings settings) {
    if (settings.name == HomeView.route)
      return GetPageRoute(settings: settings, page: () => HomeView(), binding: homeBinding);

    if (settings.name == SettingsView.route)
      return GetPageRoute(settings: settings, page: () => SettingsView(), binding: settingsBinding);

    return null;
  }
}
