import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/controllers/bottom_navigation/bottom_navigation_controller.dart';
import 'package:pub_tran_exercise/globals/theme/app_colors.dart';
import 'package:pub_tran_exercise/ui/utils/page_router.dart';
import 'package:pub_tran_exercise/ui/views/home/home_view.dart';

class BottomNavigationView extends GetView<BottomNavigationController> {
  static const String route = '/bottomNavigationView';
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        appBar: AppBar(
          systemOverlayStyle: controller.systemUiOverlayStyle,
          centerTitle: true,
          elevation: 0.0,
          title: Obx(() => Text(controller.appBarTitle)),
          // actions: [AppBarActionList()],
        ),
        bottomNavigationBar: Obx(() => Container(
              decoration: BoxDecoration(
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: (Theme.of(context).brightness == Brightness.dark)
                        ? AppColors.ligthGrey.withOpacity(0.5)
                        : AppColors.faded.withOpacity(0.5),
                    blurRadius: 1,
                  ),
                ],
              ),
              child: BottomNavigationBar(
                showSelectedLabels: false,
                showUnselectedLabels: false,
                elevation: 0.0,
                iconSize: 28,
                currentIndex: controller.currentNavPageIndex,
                onTap: controller.setCurrentNavPageIndex,
                items: controller.bottomNavigationBarItems,
              ),
            )),
        body: Navigator(
          key: Get.nestedKey(1),
          initialRoute: HomeView.route,
          onGenerateRoute: PageRouter.onGenerateRoute,
        )));
  }
}
