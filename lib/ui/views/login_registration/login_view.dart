import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/controllers/login_registration/login_controller.dart';

class LoginView extends GetView<LoginController> {
  static const String route = '/login';
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
          appBar: AppBar(systemOverlayStyle: controller.systemUiOverlayStyle),
          floatingActionButton: FloatingActionButton(onPressed: controller.login, child: Text('login'.tr)),
          body: SafeArea(
            child: Stack(children: [
              Obx(() => Visibility(
                    visible: controller.isBusy,
                    child: AbsorbPointer(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  ))
            ]),
          ),
        ));
  }
}
