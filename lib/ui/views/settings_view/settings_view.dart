import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/controllers/settings/settings_controller.dart';

class SettingsView extends GetView<SettingsController> {
  static const String route = '/settings';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(onPressed: controller.logout, child: Text('logout'.tr)),
      body: SafeArea(
        child: Stack(children: [
          Container(
            width: double.maxFinite,
            padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 15.0),
          ),
          Obx(() => Visibility(
                visible: controller.isBusy,
                child: AbsorbPointer(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ))
        ]),
      ),
    );
  }
}
