import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/globals/theme/app_theme.dart';
import 'package:pub_tran_exercise/ui/utils/initial_config.dart';
import 'package:pub_tran_exercise/ui/utils/page_router.dart';
import 'package:pub_tran_exercise/ui/views/login_registration/login_view.dart';

Future<GetMaterialApp> rootWidget(String initialRoute) async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialConfig();

  return GetMaterialApp(
    theme: AppTheme().lightTheme,
    initialRoute: initialRoute,
    getPages: PageRouter.getPages,
  );
}

Future<void> setupWidget(WidgetTester tester, {String initialRoute = LoginView.route}) async {
  await tester.pumpWidget(await rootWidget(initialRoute));

  await tester.pumpAndSettle();
}
