import 'package:get/get.dart';
import 'package:pub_tran_exercise/core/contracts/general/api_auth_provider_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/authentication_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/connectivity_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/dialog_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/navigation_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/storage_base.dart';
import 'package:pub_tran_exercise/core/controllers/home_controller.dart';
import 'package:pub_tran_exercise/globals/theme/app_theme.dart';

import 'mock_instances.dart';

BindingsBuilder initialMockBindings = BindingsBuilder(() async {
  await Get.putAsync<StorageBase>(() async => await StorageServiceMock().init());
  Get.put<NavigationBase>(NavigationServiceMock(), permanent: true);
  Get.put<DialogBase>(DialogServiceMock(), permanent: true);
  Get.put<AppTheme>(AppThemeMock(), permanent: true);
  Get.put<ConnectivityBase>(ConnectivityServiceMock(), permanent: true);
  Get.put<AuthenticationBase>(AuthenticationServiceMock(), permanent: true);
  Get.lazyPut<ApiAuthProviderBase>(() => ApiAuthProviderServiceMock(), fenix: true);
});

BindingsBuilder homeViewMockBindings = BindingsBuilder(() {
  Get.lazyPut<HomeController>(() => HomeController());
});
