import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

var log = <MethodCall>[];

void setUpGetStorageMockChannel() {
  const channel = MethodChannel('plugins.flutter.io/path_provider');
  TestDefaultBinaryMessengerBinding.instance?.defaultBinaryMessenger.setMockMethodCallHandler(
    channel,
    (MethodCall? methodCall) async {
      if (methodCall?.method == 'getApplicationDocumentsDirectory') {
        return '.';
      }
    },
  );
}
