import 'package:mockito/mockito.dart';
import 'package:pub_tran_exercise/core/contracts/general/api_auth_provider_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/authentication_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/connectivity_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/dialog_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/navigation_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/storage_base.dart';
import 'package:get/get.dart';
import 'package:pub_tran_exercise/globals/theme/app_theme.dart';

class ApiAuthProviderServiceMock extends GetxService with Mock implements ApiAuthProviderBase {}

class ConnectivityServiceMock extends GetxService with Mock implements ConnectivityBase {}

class StorageServiceMock extends GetxService with Mock implements StorageBase {}

class NavigationServiceMock extends GetxService with Mock implements NavigationBase {}

class DialogServiceMock extends GetxService with Mock implements DialogBase {}

class AppThemeMock extends Mock implements AppTheme {}

class AuthenticationServiceMock extends GetxService with Mock implements AuthenticationBase {}
