import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mockito/mockito.dart';
import 'package:pub_tran_exercise/core/contracts/general/api_auth_provider_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/connectivity_base.dart';
import 'package:pub_tran_exercise/core/contracts/general/storage_base.dart';

import 'mock_bindings.dart';
import 'mock_chanels.dart';

Future<ApiAuthProviderBase> resetStubAndGetApiAuthProviderMock(
    {Map<String, dynamic>? getAsyncMockCallback, required String path}) async {
  ApiAuthProviderBase service = Get.find();
  reset(service);

  // stubbing
  if (getAsyncMockCallback != null) {
    when(service.getAsync(path: path)).thenAnswer((_) async => getAsyncMockCallback);
  }
  return service;
}

Future<ConnectivityBase> resetStubAndGetConnectivityServiceMock({bool isConnected = true}) async {
  ConnectivityBase service = Get.find();
  reset(service);

  // stubbing
  when(service.isConnected).thenAnswer((_) async => isConnected);

  return service;
}

Future<void> registerServices() async {
  try {
    Get.testMode = true;

    initialMockBindings.builder();
    homeViewMockBindings.builder();
  } catch (e) {
    print(e.toString());
  }
}

Future<void> unregisterServices() async {
  await Get.find<StorageBase>().erase();
  Get.reset();
}

void registerChannels() {
  setUpGetStorageMockChannel();
}
